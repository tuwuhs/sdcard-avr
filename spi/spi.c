/* SPI peripheral function for AVR
 * Tested with ATMega8
 *
 * (c) 2012 Tuwuh Sarwoprasojo
 * All rights reserved
 */

/**
* \file SPI peripheral functions
* \author Tuwuh Sarwoprasojo
* \date 1 June 2012
**/

#include <avr/io.h>

#include "spi.h"

/** \brief Initialize SPI peripheral as master
 */
void spi_m_init(void)
{
	DDR_SPI |= _BV(SPI_MOSI) | _BV(SPI_SCK) | _BV(SPI_SS);
	DDR_SPI &= ~_BV(SPI_MISO);
	
	SPCR = _BV(SPE) | _BV(MSTR);
	
#ifdef SPI_LSB_FIRST
	SPCR |= _BV(DORD);
#endif
	
#if (SPI_MODE == 0)
#elif (SPI_MODE == 1)
	SPCR |= _BV(CPHA);
#elif (SPI_MODE == 2)
	SPCR |= _BV(CPOL);
#elif (SPI_MODE == 3)
	SPCR |= _BV(CPOL) | _BV(CPHA);
#else
#	error "Illegal SPI_MODE value"
#endif
	
#if (SPI_PRESCALER == 4)
#elif (SPI_PRESCALER == 16)
	SPCR |= _BV(SPR0);
#elif (SPI_PRESCALER == 64)
	SPCR |= _BV(SPR1);
#elif (SPI_PRESCALER == 128)
	SPCR |= _BV(SPR1) | _BV(SPR0);
#else
#	error "Illegal SPI_PRESCALER value"
#endif
	
#ifdef SPI_DOUBLE_SPEED
	SPSR |= _BV(SPI2X);
#endif
	

}

#ifdef SPI_HALF_DUPLEX
/** \brief Transmit one byte data through SPI as master (half-duplex)
 *  \param data Data to transmit
 */
void spi_m_tx(uint8_t data)
{
	/* - Start transmission
	 * - Wait for transmission to complete
	 */
	SPDR = data;
	while (!(SPSR & _BV(SPIF)));
}

/** \brief Receive one byte data through SPI as master (half-duplex)
 *  \return Received data
 */
uint8_t spi_m_rx(void)
{
	/* - Start transmission dummy byte
	 * - Wait for transmission to complete
	 * - Get data register
	 */
	SPDR = 0xFF;
	while (!(SPSR & _BV(SPIF)));
	return SPDR;
}

#else /* SPI_HALF_DUPLEX */

/** \brief Transmit and receive one byte data through SPI as master (full-duplex)
 *  \param tx_data Data to transmit
 *  \return Received data
 */
uint8_t spi_m_tx_rx(uint8_t tx_data)
{
	/* - Start transmission 
	 * - Wait for transmission to complete 
	 * - Get data register
	 */
	SPDR = tx_data;
	while (!(SPSR * _BV(SPIF)));
	return SPDR;
}

#endif /* SPI_HALF_DUPLEX */