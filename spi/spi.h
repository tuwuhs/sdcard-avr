/* SPI peripheral function for AVR
 * Tested with ATMega8
 *
 * (c) 2012 Tuwuh Sarwoprasojo
 * All rights reserved
 */

/**
* \file SPI peripheral functions, header file
* \author Tuwuh Sarwoprasojo
* \date 1 June 2012
**/

#ifndef SPI_H__
#	define SPI_H__

#ifndef RETCODE_SUCCESS
#	define RETCODE_SUCCESS         0x00
#endif

#ifndef TRUE
#	define TRUE    (1 == 1)
#endif
#ifndef FALSE
#	define FALSE   (1 == 0)
#endif

/* Change this depending on hardware pin configuration
 * For ATMega8: SCK=PB5, MISO=PB4, MOSI=PB3, SS=PB2
 */
#define PORT_SPI                   PORTB
#define DDR_SPI                    DDRB
#define SPI_SCK                    5
#define SPI_MISO                   4
#define SPI_MOSI                   3
#define SPI_SS                     2

/* Define prescaler (before multiply-by-2 if double speed is enabled)
 * Legal values: 4, 16, 64, 128
 */
#define SPI_PRESCALER              4
 
/* Define the CPOL and CPHA
 * SPI_MODE = (CPOL:CPHA)
 *   CPOL = 1 -- idle SCK is high (leading edge = falling, negedge)
 *   CPHA = 1 -- data is sampled at trailing edge
 */
#define SPI_MODE                   0

/* Define to use SPI double speed mode */
/*
#define SPI_DOUBLE_SPEED
*/

/* Define to set data order to LSB first */
/*
#define SPI_LSB_FIRST
*/

/* Define to use half duplex functions */
/*
#define SPI_HALF_DUPLEX
*/

#define SPI_RETCODE_SUCCESS        0x00


#define SPI_SET_SS() do {PORT_SPI &= ~_BV(SPI_SS);} while (0)
#define SPI_CLEAR_SS() do {PORT_SPI |=  _BV(SPI_SS);} while (0)

void spi_m_init(void);

#ifdef SPI_HALF_DUPLEX
void spi_m_tx(uint8_t data);
uint8_t spi_m_rx(void);

#else /* SPI_HALF_DUPLEX */
uint8_t spi_m_tx_rx(uint8_t tx_data);

#endif /* SPI_HALF_DUPLEX */

#endif /* SPI_H__ */
