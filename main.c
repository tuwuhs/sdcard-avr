/*
 * main.c
 *
 *  Created on: Mar 15, 2013
 *      Author: LENOVO
 *
 *  (c) 2013 Tuwuh Sarwoprasojo.
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "uart.h"
#include "diskio.h"
#include "ff.h"

#define TICK_PERIOD_MILLIS     (10)
#define TIMER0_PRESCALER       (1024)
#define TIMER0_RELOAD          (-(TICK_PERIOD_MILLIS * F_CPU / TIMER0_PRESCALER / 1000 + 1))

#define LINE_BUFFER_SIZE     128


volatile uint8_t g_timer_tick;

// FatFs
static FATFS FATFS_obj;
FIL fp;

// UART input line buffer
char line_buffer[LINE_BUFFER_SIZE];
uint8_t line_buffer_filled = 0;
char *p_line_buffer = line_buffer;

ISR(TIMER0_OVF_vect)
{
	TCNT0 = (uint8_t) TIMER0_RELOAD;
	g_timer_tick++;
	disk_timerproc();
}

void wait_ms(uint16_t ms)
{
	uint8_t end_timer_tick = ms/TICK_PERIOD_MILLIS;
	g_timer_tick = 0;
	while (g_timer_tick < end_timer_tick);
}

int main(void)
{
	// Setup Timer0, prescaler 1024
	TCNT0 = (uint8_t) TIMER0_RELOAD;
	TCCR0B = (1<< CS02) | (1<< CS00);
	TIMSK0 |= (1<< TOIE0);

	sei();

	DDRD = (1<< 4);

	uart_init();

	while (1) {
		if (uart_getchar(p_line_buffer) == UART_RETCODE_SUCCESS) {
			/* Replace \r or \n with \0, then raise the flag */
			if (*p_line_buffer == '\r' || *p_line_buffer == '\n') {
				*p_line_buffer = '\0';
				p_line_buffer = line_buffer;
				line_buffer_filled = 1;
			} else {
				p_line_buffer++;
				/* Buffer overflow is handled by rollover */
				if (p_line_buffer - line_buffer > LINE_BUFFER_SIZE) {
					p_line_buffer = line_buffer;
				}
			}
		}

		if (line_buffer_filled) {
			line_buffer_filled = 0;
			uart_puts(line_buffer);
			uart_puts("\n\r");
			PORTD ^= (1<< 4);

			if (strcmp("init", line_buffer) == 0) {
				uart_puts("Initializing...\n\r");
				DSTATUS status = disk_initialize(0);
				sprintf(line_buffer, "Status: %02x\n\r", status);
				uart_puts(line_buffer);
			} else if (strcmp("read", line_buffer) == 0) {
				BYTE read_buffer[512];
				DRESULT result = disk_read(0, read_buffer, 1, 1);
				int i;
				sprintf(line_buffer, "Result: %02x\n\r", result);
				uart_puts(line_buffer);
				for (i = 0; i < 512; i+=8) {
					sprintf(line_buffer, "%02x %02x %02x %02x %02x %02x %02x %02x\n\r",
							read_buffer[i], read_buffer[i+1], read_buffer[i+2], read_buffer[i+3],
							read_buffer[i+4], read_buffer[i+5], read_buffer[i+6], read_buffer[i+7]);
					uart_puts(line_buffer);
				}
			} else if (strcmp("mount", line_buffer) == 0) {
				DSTATUS dstatus;
				FRESULT fresult;

				uart_puts("Initializing...\n\r");
				dstatus = disk_initialize(0);
				sprintf(line_buffer, "Status: %02x\n\r", dstatus);
				uart_puts(line_buffer);

				uart_puts("Mounting...\n\r");
				fresult = f_mount(0, &FATFS_obj);
				sprintf(line_buffer, "Result: %02x\n\r", fresult);
				uart_puts(line_buffer);
			} else if (strcmp("write", line_buffer) == 0) {
				FRESULT fresult;
				int ret;

				uart_puts("Opening file /tuwuh.txt ...\n\r");
				fresult = f_open(&fp, "/tuwuh.txt", FA_WRITE);
				sprintf(line_buffer, "Result: %02x\n\r", fresult);
				uart_puts(line_buffer);

				if (fresult == FR_NO_FILE) {
					uart_puts("Creating file /tuwuh.txt ...\n\r");
					fresult = f_open(&fp, "/tuwuh.txt", FA_WRITE | FA_CREATE_NEW);
					sprintf(line_buffer, "Result: %02x\n\r", fresult);
					uart_puts(line_buffer);
				}

				uart_puts("Moving to end of file...\n\r");
			    ret = f_lseek(&fp, f_size(&fp));
				sprintf(line_buffer, "Result: %d\n\r", ret);
				uart_puts(line_buffer);

				uart_puts("Writing to file...\n\r");
				ret = f_printf(&fp, "Hello world! %X\n\r", rand());
				sprintf(line_buffer, "Result: %d\n\r", ret);
				uart_puts(line_buffer);

				uart_puts("Closing file /tuwuh.txt ...\n\r");
				fresult = f_close(&fp);
				sprintf(line_buffer, "Result: %02x\n\r", fresult);
				uart_puts(line_buffer);
			}
		}
	}

	return 0;
}
